public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");
        int sum = sumNumbersV1();
        System.out.println("Tổng 100 số tự nhiên = " + sum);

        int[]number1 = new int[] {1, 5, 10};
        int sum2 = sumNumbersV2(number1);
        System.out.println("Tổng của mảng number1 = " + sum2);
        printHello(99);
        printHello(20);
    }

    public static int sumNumbersV1(){
        int sum = 0;
        for (int i = 1; i <= 100; i++){
            sum = sum + i;
        }

        return sum;
    }

    public static int sumNumbersV2(int[] array){
        int sum = 0;
        for (int i = 0; i < array.length; i++){
            sum  += array[i];
        }

        return sum;
    }

    public static void printHello(int number){
        if(number <=0 ){
            System.out.println("Đây là số nhỏ hon hoặc bằng 0");;
        }else{
            if(number % 2 == 0){
                System.out.println("Đây là số chẵn");
            }
            else{
                System.out.println("Đây là số lẻ");
            }
        }
    }
}
